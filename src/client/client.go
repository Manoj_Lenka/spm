package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/shirou/gopsutil/cpu"
)

func setCpuPer(url string) {
	cpuPer, err := cpu.Percent(10, true)
	if err != nil {
		fmt.Println(err)
		return
	}
	cpuMap := map[string][]float64{
		"cpuPer": cpuPer,
	}

	jsonStr, _ := json.Marshal(cpuMap)
	fmt.Println(string(jsonStr))

	_, err = http.Post(url, "application/json", bytes.NewBuffer(jsonStr))

	if err != nil {
		panic(err)
	}

}

func main() {
	url := "http://localhost:9090/GetCpuUsage"

	for {
		<-time.After(10 * time.Second)
		go setCpuPer(url)
	}

}
