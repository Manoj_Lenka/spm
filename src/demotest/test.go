package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
)

func InitialCall(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "API is online") // send data to client side
}

func GetCpuUsage(w http.ResponseWriter, r *http.Request) {
	requestDump, err := httputil.DumpRequest(r, true)
	if err != nil {
		log.Println(err)
	}
	log.Println(string(requestDump))
}

func main() {
	http.HandleFunc("/", InitialCall)
	http.HandleFunc("/GetCpuUsage", GetCpuUsage)
	err := http.ListenAndServe(":9090", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
